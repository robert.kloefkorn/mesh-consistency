// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef MESH_CONSISTENCY_INCLUDE_ELEMENT_TYPES_HH
#define MESH_CONSISTENCY_INCLUDE_ELEMENT_TYPES_HH

namespace MeshConsistency {
  /*
   * @brief Run time element type.
   * Used to choose algorithm at run time.
   */
  enum class ElementType { triangle, quadrilateral, tetrahedron, hexahedron };

  constexpr auto quadrilateralType = ElementType::quadrilateral;
  constexpr auto hexahedronType = ElementType::hexahedron;

  constexpr auto triangleType = ElementType::triangle;
  constexpr auto tetrahedronType = ElementType::tetrahedron;

  /*
   * @brief Compile time element type.
   * Used to choose alorithm at compile time.
   */
  template<std::size_t dim>
  struct TensorElementTag {};

  template<std::size_t dim>
  struct SimplexElementTag {};

  constexpr auto quadrilateralTag = TensorElementTag<2>();
  constexpr auto hexahedronTag = TensorElementTag<3>();

  constexpr auto triangleTag = SimplexElementTag<2>();
  constexpr auto tetrahedronTag = SimplexElementTag<3>();
}

#endif //MESH_CONSISTENCY_INCLUDE_ELEMENT_TYPES_HH
