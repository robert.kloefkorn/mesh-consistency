// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef MESH_CONSISTENCY_INCLUDE_IMPL_SORT_NODES_HH
#define MESH_CONSISTENCY_INCLUDE_IMPL_SORT_NODES_HH

#include "elements.hh"

namespace MeshConsistency {
  namespace impl {
    //! sorts the 4 global indices of the nodes of the tetrahedron in ascending order
    template<typename NodeList>
    void sort_nodes(NodeList &l, const impl::Tetrahedron &) {
      if (l[0] > l[1]) std::swap(l[0], l[1]);
      if (l[2] > l[3]) std::swap(l[2], l[3]);
      if (l[0] > l[2]) std::swap(l[0], l[2]);
      if (l[1] > l[3]) std::swap(l[1], l[3]);
      if (l[1] > l[2]) std::swap(l[1], l[2]);
    }

    //! sorts the 3 global indices of the nodes of the triangle in ascending order
    template<typename NodeList>
    void sort_nodes(NodeList &l, const impl::Triangle &) {
      if (l[0] > l[1]) std::swap(l[0], l[1]);
      if (l[0] > l[2]) std::swap(l[0], l[2]);
      if (l[1] > l[2]) std::swap(l[1], l[2]);
    }
  }
}

#endif //MESH_CONSISTENCY_INCLUDE_IMPL_SORT_NODES_HH
