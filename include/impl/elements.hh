// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef MESH_CONSISTENCY_INCLUDE_IMPL_ELEMENTS_HH
#define MESH_CONSISTENCY_INCLUDE_IMPL_ELEMENTS_HH

#include <vector>
#include "element_traits.hh"
#include "edge.hh"

namespace MeshConsistency {
  namespace impl {

    /*
     * @brief Represents a quadrilateral or hexahedron element.
     * @tparam dim dimension of element.
     */
    template<std::size_t dim>
    struct TensorElement {
      //! Geometry information of the element.
      using Traits = impl::TensorElementTraits<dim>;

      std::array<std::size_t, Traits::nodes_per_element> nodes = {};
      std::array<impl::LocalEdge<TensorElement<dim>>, Traits::edges_per_element> localEdges = {};

      TensorElement() = default;
    };

    /*
     * @brief Represents a triangle or tetrahedron element.
     * @tparam dim dimension of element.
     */
    template<std::size_t dim>
    struct SimplexElement {};

    using Quadrilateral = impl::TensorElement<2>;
    using Hexahedron = impl::TensorElement<3>;

    using Triangle = impl::SimplexElement<2>;
    using Tetrahedron = impl::SimplexElement<3>;
  }
}

#endif //MESH_CONSISTENCY_INCLUDE_IMPL_ELEMENTS_HH
