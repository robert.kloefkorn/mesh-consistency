# Mesh Consistency

This header only library reorients mesh elements, such that the mesh is consistently oriented.

A mesh is consistently oriented, if all elements sharing a subentity, e.g. edges, faces, etc., use the same local coordinate
system for the shared subentity. Irregardless of the dimension, it is sufficient to orient the edges of the mesh consistently, i.e.
elements, which share an edge, agree on their local coordinate systems for the edge.
This implies that the local coordinate system on shared faces is also the same in 3D. For simplicial meshes, a consistent orientation
can be achieved by numbering the nodes of all elements ascendingly according to the global indices of the nodes. The approach for
quadrilateral and hexahedral meshes is more complex. The papers [1][1] and [2][2] give an detailed description of the algorithm used here
in these cases.

## Requirements
### Compiler
To use this library you need a C++14 compatible compiler.

### Mesh
Any simplicial mesh can be consistently oriented. The requirements on quadrilateral and especially hexahedral meshes are more restrictive.
The mesh can be oriented if it is one of the following types:
* A 2d orientable manifold.
* An extruded 2d mesh.
* A mesh resulting from subdividing tetrahedra into hexahedra.

If none of these cases apply, a consistent orientation may not exist. In that case, an exception is thrown. Refining this mesh globally
once results in a mesh which can always be consistently oriented.

### User
After computing a consistent orientation, the user may not reorient the elements. For quadrilateral and hexahedral mesh this can be relaxed.
In these cases, the volume of the elements is always positive, therefore the user is only required to keep the orientation of elements with
positive volume.

## Installation Guide
The library can be installed with these steps
```
$ git clone https://zivgitlab.uni-muenster.de/marckoch/consistent-edge-orientation.git
$ cd consistent-edge-orientation
$ mkdir build && cd build
# Optionally tests may be run
$ make && make test
$ make install
```

## Usage Guide
To compute correctly oriented elements you need to include only `mesh-consistency.hh`. An exaple usage is
given in the following:
```c++
#include <consistent-edge-orientation/consistent-edge-orientation.hh>

std::vector<Vertex> vertices = /* initialize vertices */;

std::vector<Element> elements = /* initialize elements */;

MeshConsistency::ElementType type = /* deduce runtime element type */;

MeshConsistency::orient_consistently(vertices, elements, type);
```
`Vertex` symbolizes a position in 2D or 3D. `Element` is assumed to hold a list of
global indices of vertices. Both types need to support `begin()` and `end()` iterators, `size()` and the `[]` operator.
The local coordinate system for quadrilaterals is
given by
```
2----------3
|          |
|          |
|          |
0----------1
```
and for hexahedra it is the corresponding generalization. `ElementType` is defined in `element_types.hh`, although you do not
need to include it separatly. For each element type exists a constant `ElementTyp` object.

Alternatively, there are constant tags to pick the right algorithm at compile time, also defined in `element_types.hh`.
These are used in the same place as the `ElementType` objects, i.e.
```c++
MeshConsistency::orient_consistently(vertices, elements, MeshConsistency::quadrilateralTag);
```

## License
This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).

[1]: https://doi.org/10.1145/3061708
[2]: https://doi.org/10.1137/15M1021325
