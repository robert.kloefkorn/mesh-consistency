// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <cassert>
#include <map>
#include <iostream>
#include "mesh_io.hh"
#include <string>
#include <memory>
#include <cmath>
#include "mesh-consistency.hh"
#include "mesh_io.hh"
#include "impl/orientation.hh"

using namespace MeshConsistency;
using namespace impl;

using Node = std::array<double, 3>;
using NodeList = std::vector<std::size_t>;

template<typename E>
bool test_consistent_orientation(const std::vector<Node> &nodes,
                                 const std::vector<NodeList> &node_lists){
  auto elements = init_elements(node_lists, E());

  auto edges = init_global_edges(elements);

  return is_consistently_oriented(edges);
}

bool test_consistent_orientation(const std::vector<Node> &nodes,
                                 const std::vector<NodeList> &node_lists){
  if (node_lists.empty()) return true;

  const std::size_t nnodes = node_lists.begin()->size();

  if(nnodes == 4)
    return test_consistent_orientation<Quadrilateral>(nodes, node_lists);
  else if(nnodes == 8)
    return test_consistent_orientation<Hexahedron>(nodes, node_lists);
  else
    throw std::runtime_error("Element type with " + std::to_string(nnodes) + " nodes not recognized");
}


void test_simplex(){
  std::string fileNames[] = {"missingcorner3dtet", "unstructuredtri"};

  for(const auto& file : fileNames) {
    auto ret = read_mesh("mesh/" + file);

    orient_consistently(std::get<0>(ret), std::get<1>(ret), std::get<2>(ret));

    for (auto &l : std::get<1>(ret))
      assert(std::is_sorted(l.begin(), l.end()));
  }
}


void test_consistent(){
  const std::string file = "mesh/square_consistent";

  auto ret = read_mesh(file);

  assert(test_consistent_orientation(std::get<0>(ret), std::get<1>(ret)));
}

void test_not_consistent(){
  std::string fileNames[] = {"torus", "moebius-band"};

  for(const auto& file : fileNames) {
    auto ret = read_mesh("mesh/" + file);

    assert(not(test_consistent_orientation(std::get<0>(ret), std::get<1>(ret))));

    try {
      orient_consistently(std::get<0>(ret), std::get<1>(ret), std::get<2>(ret));
      std::cout << "Found consistent orientation with moebius band" << std::endl;
      assert(false);
    } catch (NotOrientable &e) {}
  }
}

void test_consistent_after_reorder(){
  std::string fileNames[] = {"square", "square_consistent", "unstructured", "cube",
                             "refined-torus", "missingcorner3d", "diamond", "sphere",
                             "periodic"};

  for(const auto& file : fileNames){
    auto ret = read_mesh("mesh/" + file);

    orient_consistently(std::get<0>(ret), std::get<1>(ret), std::get<2>(ret));

    assert(test_consistent_orientation(std::get<0>(ret), std::get<1>(ret)));
  }
}

int main(int argc, char** argv){
  test_consistent();
  test_not_consistent();
  test_consistent_after_reorder();
  test_simplex();
}
