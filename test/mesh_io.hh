// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef CONSISTENT_EDGE_ORIENTATION_SRC_MESH_IO_HH
#define CONSISTENT_EDGE_ORIENTATION_SRC_MESH_IO_HH

#include <fstream>
#include <string>
#include <vector>
#include "mesh-consistency.hh"

using Node = std::array<double, 3>;
using NodeList = std::vector<std::size_t>;

auto read_mesh(const std::string &fileName){
  using namespace MeshConsistency;
  using namespace impl;

  std::ifstream file(fileName);

  std::string line;

  file >> line;
  if(line != "nodes")
    throw std::runtime_error("Wrong file format.");

  std::size_t nnodes;
  file >> nnodes;

  std::vector<Node> nodes(nnodes);
  for(auto& node : nodes)
    file >> node[0] >> node[1] >> node[2];

  file >> line;
  ElementType type;
  std::vector<std::size_t> elementNodes;
  if(line == "quadrilateral") {
    type = quadrilateralType;
    elementNodes.resize(4);
  }
  else if(line == "hexahedron") {
    type = hexahedronType;
    elementNodes.resize(8);
  }
  else if(line == "triangle") {
    type = triangleType;
    elementNodes.resize(3);
  }
  else if(line == "tetrahedron") {
    type = tetrahedronType;
    elementNodes.resize(4);
  }
  else
    throw std::runtime_error("Wrong file format.");

  std::size_t nelements;
  file >> nelements;

  std::vector<std::vector<std::size_t>> elements(nelements);
  for(auto& element: elements) {
    for(auto& elementNode : elementNodes)
      file >> elementNode;
    element = elementNodes;
  }

  return std::make_tuple(nodes, elements, type);
}

void write_mesh(const std::string& fileName, const std::vector<Node>& nodes,
                const std::vector<NodeList>& node_lists, const MeshConsistency::ElementType type){
  using namespace MeshConsistency;
  using namespace impl;

  std::ofstream file(fileName);

  file << "nodes" << std::endl;;

  file << nodes.size() << std::endl;;

  for(auto& node : nodes)
    file << node[0] << " " << node[1] << " "  << node[2] << std::endl;

  if(type == quadrilateralType)
    file << "quadrilateral" << std::endl;
  else if(type == hexahedronType)
    file << "hexahedron" << std::endl;
  else if(type == triangleType)
    file << "triangle" << std::endl;
  else
    file << "tetrahedron" << std::endl;

  file << node_lists.size() << std::endl;

  for(auto& node_list: node_lists) {
    for(auto& node : node_list)
      file << node << " ";
    file << std::endl;
  }
}


#endif //DUNE_CONSISTENT_EDGE_ORIENTATION_SRC_MESH_IO_HH
