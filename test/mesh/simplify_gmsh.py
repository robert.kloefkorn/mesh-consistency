#!/usr/bin/env python3
import sys


type_dim = {1: 1, 2: 2, 3: 2, 4: 3, 5: 3, 15: 0}
type_names = {1: 'line', 2: 'triangle', 3: 'quadrilateral', 4: 'tetrahedron', 5: 'hexahedron', 15: 'node'}
type_nvertices = {1: 2, 2: 3, 3: 4, 4: 4, 5: 8, 15: 1}

def parse_gmsh(filename):
    def pre_check(file, section):
        line = file.readline().rstrip()
        if line != "$" + section:
            raise RuntimeError

    def post_check(file, section):
        line = file.readline().rstrip()
        if line != "$End" + section:
            raise RuntimeError

    def read_header(file):
        section = "MeshFormat"
        pre_check(file, section)

        line = file.readline()
        version, file_type, data_size = line.split()
        if float(version) < 2.0 or float(version) > 2.2 or int(file_type) != 0 or int(data_size) != 8:
            raise RuntimeError

        post_check(file, section)

    def read_nodes(file):
        section = "Nodes"
        pre_check(file, section)

        line = file.readline()
        nnodes = int(line)

        nodes = []
        for i in range(nnodes):
            line = file.readline()
            nodes.append([int(line.split()[0])] + [float(p) for p in line.split()[1:]])

        post_check(file, section)

        return nodes

    def read_elements(file):
        section = "Elements"
        pre_check(file, section)

        line = file.readline()
        nelements = int(line)

        elements = []
        for i in range(nelements):
            line = file.readline()
            elements.append([int(p) for p in line.split()])

        post_check(file, section)

        return elements


    with open(filename, 'r') as file:
        read_header(file)

        nodes = read_nodes(file)

        elements = read_elements(file)

    return nodes, elements


def find_element_type(elements):
    types = set()
    for element in elements:
        types.add(element[1])

    dim = max([type_dim[t] for t in types])

    volume_types = set()
    for t in types:
        if type_dim[t] == dim:
            volume_types.add(t)

    if len(volume_types) != 1:
        raise RuntimeError

    return volume_types.pop()


def extract_elements(elements, type):
    new_elements = []
    for element in elements:
        if element[1] == type:
            new_elements.append(element)

    return new_elements


def dune_numbering(element, type):
    new_element = element
    if type == 3:
        new_element[2], new_element[3] = new_element[3], new_element[2]
        pass
    elif type == 5:
        new_element[2], new_element[3] = new_element[3], new_element[2]
        new_element[6], new_element[7] = new_element[7], new_element[6]

    return new_element


def write_simplified_mesh(filename, nodes, elements):
    if len(elements) == 0:
        return

    type = elements[0][1]

    with open("simplified_" + filename, 'w') as file:
        file.write("nodes\n")
        file.write(str(len(nodes)) + "\n")
        for node in nodes:
            file.write(" ".join([str(p) for p in node[1:]]) + "\n")

        file.write(type_names[type] + "\n")
        file.write(str(len(elements)) + "\n")
        for element in elements:
            element_nodes = dune_numbering(element[-type_nvertices[type]:], type)
            file.write(" ".join([str(p) for p in element_nodes]) + "\n")


def compress_node_indices(nodes, elements):
    max_idx = max([n[0] for n in nodes]) + 1

    idx_to_compressed_idx = [0] * max_idx

    new_nodes = []
    for i, node in enumerate(nodes):
        idx_to_compressed_idx[node[0]] = i
        new_nodes.append([i] + node[1:])

    new_elements = []
    for element in elements:
        nvertices = type_nvertices[element[1]]
        element_nodes = element[-nvertices:]
        compressed_nodes = [idx_to_compressed_idx[n] for n in element_nodes]
        new_elements.append(element[:-nvertices] + compressed_nodes)

    return new_nodes, new_elements


def simplify_gmsh(filename):
    nodes, elements = parse_gmsh(filename)

    compressed_nodes, compressed_elements = compress_node_indices(nodes, elements)

    volume_type = find_element_type(compressed_elements)

    volume_elements = extract_elements(compressed_elements, volume_type)

    write_simplified_mesh(filename, compressed_nodes, volume_elements)


if __name__ == "__main__":
    simplify_gmsh(sys.argv[1])
